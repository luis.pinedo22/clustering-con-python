# Clustering con Python

_Se realiza un algoritmo no supervisado para la agrupación de datos de un dataset y de datos aleatorios._


## Contenido

1. [¿Qué es un clustering?](#%C2%BFQu%C3%A9%20es%20un%20clustering?)
2. [¿Cómo funciona?](#c%C3%B3mo-funciona)
3. [Comenzando](#comenzando)
	* [Pre-requisitos](#pre-requisitos)
	* [Requerimientos del sistema](#requerimientos-de-sistema)
4. [Instalación](#instalaci%C3%B3n)
5. [Ejecución](#ejecuci%C3%B3n)
6. [Librerías](#librer%C3%ADas)
7. [Métodos](#m%C3%A9todos)
8. [Ejemplos](#ejemplos)
9. [Autores](#autores)
10. [Referencias](#referencias)



## ¿Qué es un clustering?
> Un clustering categoriza los datos de un conjunto de datos 
> en clusters, segmentos o grupos, cada uno de los cluster 
> contiene datos que son similares o tienen ciertas características
> en común. Cada uno de los cluster se trata de manera separada, 
> se puede definir un plan para cada uno de ellos.

![](imagenes/cluster_num_aleatorios.png)

## ¿Cómo funciona?

![](imagenes/cluster.gif)


## Comenzando

_Si deseas obtener este repositorio lo puedes hacer de las siguientes maneras:_

* Hacer un Fork.

![](imagenes/fork.jpg)

* Descargarlo directamente a tu equipo.

![](imagenes/descargar.jpg)

* Clonar el repositorio con [Git](https://git-scm.com/).
	* Si no tienes Git instalado en tu equipo lo puedes descargar desde su página oficial de [Git](https://git-scm.com/).


### Pre-requisitos

_Para poder hacer la ejecución de los códigos necesitas las siguientes herramientas:_

* [Anaconda Navigator](https://www.anaconda.com) - Está disponible para los diferentes sistemas operativos, arquitecturas y versiones de Python.
	* Windows
		* Versión 3.7 de Python
			* [64-Bit Graphical Installer (631 MB)](https://repo.anaconda.com/archive/Anaconda3-5.3.0-Windows-x86_64.exe)
			* [32-Bit Graphical Installer (509 MB)](https://repo.anaconda.com/archive/Anaconda3-5.3.0-Windows-x86.exe)
		* Versión 2.7 de Python
			* [64-Bit Graphical Installer (579 MB)](https://repo.anaconda.com/archive/Anaconda2-5.3.0-Windows-x86_64.exe)
			* [32-Bit Graphical Installer (457 MB)](https://repo.anaconda.com/archive/Anaconda2-5.3.0-Windows-x86.exe)
	* MacOS
		* Versión 3.7 de Python
			* [64-Bit Graphical Installer (634 MB)](https://repo.anaconda.com/archive/Anaconda3-5.3.0-MacOSX-x86_64.pkg)
			* [64-Bit Command-Line Installer (544 MB)](https://repo.anaconda.com/archive/Anaconda3-5.3.0-MacOSX-x86_64.sh)
		* Versión 2.7 de Python
			* [64-Bit Graphical Installer (628 MB)](https://repo.anaconda.com/archive/Anaconda2-5.3.0-MacOSX-x86_64.pkg)
			* [64-Bit Command-Line Installer (539 MB)](https://repo.anaconda.com/archive/Anaconda2-5.3.0-MacOSX-x86_64.sh)
	* Linux
		* Versión 3.7 de Python
			* [64-Bit (x86) Installer (637 MB)](https://repo.anaconda.com/archive/Anaconda3-5.3.0-Linux-x86_64.sh)
			* [64-Bit (Power8 and Power9) Installer (305 MB)](https://repo.anaconda.com/archive/Anaconda3-5.3.0-Linux-ppc64le.sh)
			* [32-Bit Installer (527 MB)](https://repo.anaconda.com/archive/Anaconda3-5.3.0-Linux-x86.sh)
		* Versión 2.7 de Python
			* [64-Bit (x86) Installer (618 MB)](https://repo.anaconda.com/archive/Anaconda2-5.3.0-Linux-x86_64.sh)
			* [64-Bit (Power8 and Power9) Installer (286 MB)](https://repo.anaconda.com/archive/Anaconda2-5.3.0-Linux-ppc64le.sh)
			* [32-Bit Installer (508 MB)](https://repo.anaconda.com/archive/Anaconda2-5.3.0-Linux-x86.sh)
* [Python](https://www.python.org/)
	* Windows
		* [Versión 3.7](https://www.python.org/downloads/release/python-370/)
		* [Versión 2.7](https://www.python.org/downloads/release/python-2715/)
	* MacOS
		* [Versión 3.7](https://www.python.org/downloads/release/python-370/)
		* [Versión 2.7](https://www.python.org/downloads/release/python-2715/)
	* Otras plataformas
		* [Versión 3.7](https://www.python.org/downloads/release/python-370/)
		* [Versión 2.7](https://www.python.org/downloads/release/python-2715/)

```
Si haces la instalación con Anaconda Navigator no necesitas descargar Python.
```


### Requerimientos de sistema 

* **Sistema operativo** - Windows Vista o más reciente, macOS 10.10+ de 64 bits, o Linux, incluyendo Ubuntu, RedHat, CentOS 6+ y otros.
* **Arquitectura del sistema** - x86 de 64 bits, x86 de 32 bits con Windows o Linux, Power8 o Power9.
* **Espacio mínimo en disco** - 3 GB para descargar e instalar.



## Instalación

1. Descarga e instala Anaconda Navigator para el sistema operativo que tengas, pero en la versión de Python 3.7.
	* Si estas en Windows solo busca el archivo y ejecuta el .exe.
	* Si estas en MacOS y descargaste el archivo con extensión .pkg solo ejecuta el archivo.
	* Si estas en MacOS y descargaste el archivo con extensión .sh abre una terminal, situarte en la carpeta de donde se encuentra el archivo y ejecuta el siguiente comando para hacer la instalación de Anaconda Navigator:
		
		<code>sh nombre_del_Archivo.sh<code>

		Por ejemplo
		
		* Para la versión 3.7
			
			<code> sh ~/Downloads/Anaconda3-5.3.0-MacOSX-x86_64.sh<code>

		* Para la versión 2.7
		
			<code> sh ~/Downloads/Anaconda2-5.3.0-MacOSX-x86_64.sh<code>

	* Si estas en Linux solo busca el archivo, abre una terminal, situarte en la carpeta de donde se encuentra el archivo y ejecuta el siguiente comando para hacer la instalación de Anaconda Navigator:
		
		<code>sh nombre_del_Archivo.sh<code>

		Por ejemplo
		
		* Para la versión 3.7
		
			<code>sh ~/Downloads/Anaconda3-5.3.0-Linux-x86_64.sh<code>

		* Para la versión 2.7
		
			<code>sh ~/Downloads/Anaconda2-5.3.0-Linux-x86_64.sh<code>

## Ejecución

1. Ejecuta Anaconda Navigator.
	* Si estas en Windows:
		* Busca el Acceso directo en el escritorio.

	* Si estas MacOS:
		* Busca el Acceso directo en el Launchpad.

	* Si estas en Linux:
		* Ejecuta la terminal y escribe lo siguiente:

			<code>export PATH=~/anaconda3/bin:$PATH
			anaconda-navigator<code>

			
2. Ejecuta Jupyter notebook.

![](imagenes/jupyter.jpg)

3. Navega entre tus directorios para encontrar los notebooks.

![](imagenes/directorio.jpg)

4. En la carpeta de 'notebook' vienen los ejemplos de clustering.

![](imagenes/ejemplos.jpg)

5. Cuando se tenga un notebook abierto se puede ejecutar el código con el botón de 'Run' o con la combinación de las teclas 'CTRL+Enter'.

![](imagenes/barra.jpg)


## Librerías

_Las librerías utilizadas en los ejemplos son las siguientes:_


* NumPy - [Documentación](http://www.numpy.org/)
* scikit-learn - [Documentación](http://scikit-learn.org/stable/)
* Matplotlib - [Documentación](https://matplotlib.org/)
* SciPy - [Documentación](https://www.scipy.org/)
* Pandas - [Documentación](https://pandas.pydata.org/)


## Métodos

_Los métodos utilizados en los ejemplos son los siguientes:_

* KMeans - [Documentación](http://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html)
* make_blobs - [Documentación](http://scikit-learn.org/stable/modules/generated/sklearn.datasets.make_blobs.html)
* vq - [Documentación](https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.vq.vq.html)
* AgglomerativeClustering - [Documentación](http://scikit-learn.org/stable/modules/generated/sklearn.cluster.AgglomerativeClustering.html)
* AffinityPropagation - [Documentación](http://scikit-learn.org/stable/modules/generated/sklearn.cluster.AffinityPropagation.html)


## Ejemplos

1. [Ejemplo 1](https://gitlab.com/luis.pinedo22/clustering-con-python/blob/master/notebook/1%20-%20Ejemplo%201.ipynb)
	* [Imagenes de los resultados](https://gitlab.com/luis.pinedo22/clustering-con-python/tree/master/imagenes/E1)
2. [Ejemplo 2](https://gitlab.com/luis.pinedo22/clustering-con-python/blob/master/notebook/2%20-%20Ejemplo%202.ipynb)
	* [Imagenes de los resultados](https://gitlab.com/luis.pinedo22/clustering-con-python/tree/master/imagenes/E2)
3. [Ejemplo 3](https://gitlab.com/luis.pinedo22/clustering-con-python/blob/master/notebook/3%20-%20Ejemplo%203.ipynb)
	* [Imagenes de los resultados](https://gitlab.com/luis.pinedo22/clustering-con-python/tree/master/imagenes/E3)
4. [Ejemplo 4](https://gitlab.com/luis.pinedo22/clustering-con-python/blob/master/notebook/4%20-%20Ejemplo%204.ipynb)
	* [Imagenes de los resultados](https://gitlab.com/luis.pinedo22/clustering-con-python/tree/master/imagenes/E4)
5. [Ejemplo 5](https://gitlab.com/luis.pinedo22/clustering-con-python/blob/master/notebook/5%20-%20Ejemplo%205.ipynb)
	* [Imagenes de los resultados](https://gitlab.com/luis.pinedo22/clustering-con-python/tree/master/imagenes/E5)


## Autores

* **Luis Angel** - [GitLab](https://gitlab.com/luis.pinedo22)

## Licencia

Bajo licencia de MIT - [Ver licencia](https://gitlab.com/luis.pinedo22/clustering-con-python/blob/master/LICENSE)


## Referencias

* scikit-learn. (2018). Demo of affinity propagation clustering algorithm — scikit-learn 0.20.0 documentation. Retrieved October 11, 2018

* Huneycutt, J. (2018). An Introduction to Clustering Algorithms in Python – Towards Data Science. Retrieved October 11, 2018
