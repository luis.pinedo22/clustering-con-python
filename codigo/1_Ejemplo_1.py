#Clustering con el método make_blobs

#Se importan las librerías necesarias
from sklearn.datasets import make_blobs
import numpy as np
import matplotlib.pyplot as plt

#Se generan los datos aleatorios y se hace el clustering
datos = make_blobs(n_samples=200, n_features=2, centers=4, cluster_std=1, random_state=15)

#Se muestra los datos y las etiquetas de los clusters en un array
datos

# Se almacenan los datos aleatorios en un array
puntos = datos[0]

# Se almacenan las etiquetas de los clusters en un array
etiquetas = datos[1]

#Se grafican los clusters
plt.scatter(puntos[:,0], puntos[:,1], c=etiquetas, cmap='viridis')
plt.xlim(-14,12)
plt.ylim(-15,7)
plt.xlabel("Eje X")
plt.ylabel("Eje Y")
plt.title("Clusters")